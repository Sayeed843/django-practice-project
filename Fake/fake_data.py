import random,os


class FakeProduct():
    pen_brand = {
        "a. t. cross company": "cross_pen",
         "lamy":"lamy_pen",
         "montegrappa":"montegrappa_pen",
         "montblanc international":"montblanc_international_pen",
         "newell brands":"newell_brands_pen",
         "s.t. dupont":"dupont_pen",
         "parker":"parker_pen",
         "sheaffer":"sheaffer_pen",
         "aurora":"aurora_pen"
            }



    pro_category_list = {
        "pen": pen_brand,
            }

    def __init__(self):
        pass

    def product_category(self):
        category, brand = random.choice(list(self.pro_category_list.items()))
        return category

    def product_name(self, category):
        brand = random.choice(list(self.pro_category_list[category]))
        return brand

    def product_pic(self, category, brand):
        path = self.pro_category_list[category][brand]
        # print("Path: "+str(os.listdir()))
        return ("Fake/pen/"+path+"/"+random.choice(os.listdir("Fake/pen/"+path)))

    def product_description(self):
        return ("Lorem ipsum dolor sit amet, consectetur adipisicing elit,\
                veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.")



    def product_barcode(self):
        pass

    def product_price(self, limit):
        return str(round(random.uniform(5.00, 200.00), limit))



    def pen_deatils(self, limit):
        pro_category = self.product_category()
        pen_brand = self.product_name(pro_category)
        pen_desc = self.product_description()
        pen_picture = self.product_pic(pro_category, pen_brand)
        pen_price = self.product_price(limit)

        product = {
            "name":pen_brand,
            "description":pen_desc,
            "category":pro_category,
            "picture":pen_picture,
            "price":pen_price
            }
        return product

if __name__=="__main__":
    pen = FakeProduct()
    # c = pen.product_category()
    # b = pen.product_name(c)
    # p =pen.product_pic(c,b)
    # pri = pen.product_price(2)
    # print("Fake Product Create For Ecommerce!")
    # print("Category: "+str(c))
    # print("Pen Name: " +str(b))
    # print("Pen Pic: "+str(p))
    # print("Price: "+str(pri))
    product = pen.pen_deatils(2)
    for key in product:
        print(key+":" +str(product[key]))
