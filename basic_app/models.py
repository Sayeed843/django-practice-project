from django.db import models
from django.contrib.auth.models import User
# Create your models here.



class UserProfileInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    profile_pic = models.ImageField(upload_to="profile-pic", blank=True)

    def __str__(self):
        return self.user.username


class ProductInfo(models.Model):
    pro_name = models.CharField(max_length=400, blank=False)
    pro_desc = models.CharField(max_length=1000, blank=True)
    pro_pic = models.ImageField(upload_to="product-pics", blank=True)
    pro_price = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return str(self.pro_name) +"  "+ str(self.pro_pic)
