from django.urls import path
from basic_app.views import registration, user_login

app_name = "basic_app"
urlpatterns=[
    path("register/", registration, name="register"),
    path("login/", user_login, name="login"),
]
