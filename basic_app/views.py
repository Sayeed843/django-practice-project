from django.shortcuts import render
from basic_app.models import ProductInfo
from basic_app.forms import UserForm, UserProfileInfoForm
# Create your views here.


from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required





def index(request):
    product = ProductInfo.objects.all()
    context_dic = {
        "product":product,
    }
    return render(request, "basic_app/index.html", context_dic)


def registration(request):
    user_form = UserForm()
    user_profile = UserProfileInfoForm()
    register = True


    if request.method == "POST":
        user_form = UserForm(request.POST)
        user_profile = UserProfileInfoForm(request.POST)

        if user_form.is_valid() and user_profile.is_valid():

            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = user_profile.save(commit=False)
            profile.user=user

            if "profiles-pic" in request.FILES:
                profile.profile_pic=request.FILES["profile-pic"]

            profile.save()
            register = True
        else:
            print(user_form.errors+ "---"+str(user_profile.error))


    context_dic = {
        "user_form":user_form,
        "user_profile":user_profile,
        "register":register,
    }

    return render(request,"basic_app/registration.html",context_dic)


@login_required
def user_loguot(request):
    logout(request)
    return HttpResponseRedirect(reverse("index"))


def user_login(request):

    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(reverse("index"))
            else:
                return HttpResponse("Account is not active")
        else:
            print("Someone try to login!")
            print("Username: {} and Password: {}".format(username,password))
            return HttpResponse("Invaild login details")


    else:
        return render(request, "basic_app/login.html")
