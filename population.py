import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "practicePro.settings")


import django
django.setup()

# from faker import Faker
from Fake.fake_data import FakeProduct
from basic_app.models import ProductInfo
from django.core.files import File

# fakergen = Faker()
fake_product = FakeProduct()

def add_product_info(pro_name, pro_desc, pro_pic, pro_price):
    pic_name = pro_pic.split("/")
    pic_name = pic_name[len(pic_name)-1]
    # print("Pic name: "+str(pic_name))

    # pro_pic=pro_pic.encode('utf-8').strip()
    print("Product Pic: "+str(pro_pic))

    # pro_name=pro_name,
    # pro_desc=pro_desc,
    # pro_price=pro_price

    product_info = ProductInfo()
    product_info.pro_name = pro_name
    product_info.peo_desc = pro_desc
    # product_info.pro_pic = (pic_name, File(open(pro_pic,"r")))
    product_info.pro_price = pro_price
    product_info.pro_pic.save(pic_name, File(open(pro_pic,"rb")))
    # return product_info

def populated(N=10):
    for entry in range(N):
        product = fake_product.pen_deatils(2)
        add_product_info(product["name"], product["description"], product["picture"], product["price"])




if __name__=="__main__":

    # print()
    print(str(fake_product.pen_deatils(2)["picture"]))
    print("Population Start")
    populated(15)
    print("Done!")
